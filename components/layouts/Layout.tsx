import React, { FC, ReactNode } from 'react'
import { Box } from '@mui/material'
import Head from 'next/head'
import { Navbar, Sidebar } from '../ui'

interface Props {
  title?: string
  children?: ReactNode
}

export const Layout: FC<Props> = ( { title, children } ) => {

  return (
    // Todo de Materal UI
    // Define un caja "sx": accede a las propiedades de los themas
    <Box sx={ { flexFlow: 1 } }> 

      {/* Cabiar la cabecera con HEAD de nextt */}
      <Head>
        <title> {title || 'OpenJira - App'} </title>
      </Head>

      {/* Navbar */}
      <Navbar />

      {/* Sidebar */}
      <Sidebar />
      {/* Los hijos de los componentes */}
      <Box sx={{padding: '.5rem .8rem'}}>
        { children }
      </Box>

    </Box>
  )
}
