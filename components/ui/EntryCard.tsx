import React, { DragEvent, FC, useContext } from 'react'
import { useRouter } from 'next/router';
import { Card, CardActionArea, CardActions, CardContent, Typography } from '@mui/material'
import { Entry } from '../../interfaces'
import { UIContext } from '../../context/ui';
import { dateFuntions } from '../../utils';

interface Props{
    entry: Entry
}

export const EntryCard: FC <Props> = ( { entry }) => {

    const { setIsDragEnd, setIsDragStart } = useContext(UIContext);


    // Usar useROuter de Next para la navegacion
    const router = useRouter();

    const { _id, createdAt, description, status } = entry;

    const onDrag = ( event: DragEvent<HTMLDivElement> ) => {
        event.dataTransfer.setData('text', _id);
        setIsDragStart();
    }

    const onDragEnd = () => {
        setIsDragEnd();
    }

    const handleClickToPage = () => {
        router.push(`/entries/${_id}`)
    }

  return (
    <Card
        sx={{marginBottom: 1}}
        // TODO: Events drag
        draggable
        onDragStart={ onDrag }
        onDragEnd={ onDragEnd }
        onClick={ handleClickToPage }
    >
        <CardActionArea>
            <CardContent>
                <Typography sx={{whiteSpace: 'pre-line'}}>
                    This is description: { description }
                </Typography>
            </CardContent>

            <CardActions sx={{
                padding: 1,
                display: 'flex',
                justifyContent: 'end'
            }}>
                <Typography variant='body2'>
                     { dateFuntions.getFormatDistanceToNow( createdAt )}
                </Typography>
            </CardActions>
        </CardActionArea>
    </Card>
  )
}
