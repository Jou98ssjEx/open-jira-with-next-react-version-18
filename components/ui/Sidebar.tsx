import { Box, Divider, Drawer, List, ListItem, ListItemIcon, ListItemText, Typography } from '@mui/material'
import InboxOutlinedIcon from '@mui/icons-material/InboxOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import { useContext } from 'react';
import { UIContext } from '../../context/ui';

const menuItems = [ 'Inbox', 'Starred', 'Send Email', 'Drafts'];


export const Sidebar = () => {

    const { closeSideMenu, sideMenuOpen} = useContext( UIContext );
  return (
      // Sidebar: 
    <Drawer
        anchor='left'
        open={ sideMenuOpen }
        onClose= { closeSideMenu }
    >
        {/* Cajas: div */}
        <Box sx={{ width: 250 }}>
            <Box sx={{padding: '10px 15px'}}>
                <Typography variant='h4'>
                    Menú
                </Typography>
            </Box>

            <List>
                {
                    menuItems.map( ( data, idx ) => (
                        <ListItem
                            button
                            key={ data }
                        >
                            <ListItemIcon>
                                {/* Par o impar */}
                                { idx % 2 ? <InboxOutlinedIcon /> : <EmailOutlinedIcon />}
                            </ListItemIcon>

                            {/* Texto */}
                            <ListItemText primary={ data } />
                        </ListItem>
                    ))
                }
            </List>

            {/* Linea de separacion como el hr */}
            <Divider />

            <List>
                {
                    menuItems.map( ( data, idx ) => (
                        <ListItem
                            button
                            key={ data }
                        >
                            <ListItemIcon>
                                {/* Par o impar */}
                                { idx % 2 ? <InboxOutlinedIcon /> : <EmailOutlinedIcon />}
                            </ListItemIcon>

                            {/* Texto */}
                            <ListItemText primary={ data } />
                        </ListItem>
                    ))
                }
            </List>
        </Box>

    </Drawer>
  )
}
