import type { NextApiRequest, NextApiResponse } from 'next'
import { db, seedData } from '../../database'
import { EntryModel } from '../../models'

type Data = {
        ok: boolean,
        msg: string
    }


export default async function (req: NextApiRequest, res: NextApiResponse<Data>) {

    if (process.env.NODE_ENV === 'production'){
        return res.status(401).json({
            ok: true,
            msg: `No autorizaed`
        })
    }

    await db.connect();

    await EntryModel.deleteMany();  // elimina todo el doc de la db

    await EntryModel.insertMany( seedData.entries );

    await db.disconnect();

    res.status(200).json({ ok: true, msg: 'Example' })
}