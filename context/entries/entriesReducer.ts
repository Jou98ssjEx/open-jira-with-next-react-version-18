import { Entry } from '../../interfaces';
import { EntriesState } from './';

type EntriesActionType = 
   | { type: '[Entry] - Add-Entry', payload: Entry}
   | { type: '[Entry] - Updated-Entry', payload: Entry}
   | { type: '[Entry] - Delete-Entry', payload: string}
   | { type: '[Entry] - Data-Entry-Loading', payload: Entry[]}

export const entriesReducer  = ( state: EntriesState, action: EntriesActionType ): EntriesState => {
   switch (action.type) {

       case '[Entry] - Add-Entry':
           return {
               ...state,
               entries: [ ...state.entries, action.payload ],
           }
           
        case '[Entry] - Updated-Entry':
            return{
                ...state,
                // entries: state.entries.map( data => (
                //     data._id === action.payload._id
                //         ? data.status = action.payload.status
                //         : data
                // ))
                entries: state.entries.map( data => {
                    if( data._id === action.payload._id ){
                        data.status = action.payload.status;
                        data.description = action.payload.description;
                    }
                    return data;
                })
            }

        case '[Entry] - Data-Entry-Loading':
            return{
                ...state,
                entries: action.payload
            }

        case '[Entry] - Delete-Entry':
            return{
                ...state,
                entries: state.entries.filter( entry => entry._id !== action.payload )
            }

       default:
          return state;
   }
} 