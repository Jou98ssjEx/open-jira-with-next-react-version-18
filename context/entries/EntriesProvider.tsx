import { useSnackbar } from 'notistack';
import { FC, useEffect, useReducer } from 'react';
// import { v4 as uuidV4 } from 'uuid';
import entryApi from '../../apis/entriesApi';
import { Entry } from '../../interfaces';
import { EntriesContext, entriesReducer } from './';
import React from 'react';



export interface EntriesState {
   entries: Entry[];
}

const Entries_INITIAL_STATE: EntriesState = {
   entries: [
      //  {
      //      _id: uuidV4(),
      //      description: 'Pending..',
      //      createdAt: Date.now(),
      //      status: 'pending',
      //  },
      //  {
      //      _id: uuidV4(),
      //      description: 'In-Progress..',
      //      createdAt: Date.now(),
      //      status: 'in-progress',
      //  },
      //  {
      //      _id: uuidV4(),
      //      description: 'Finished..',
      //      createdAt: Date.now(),
      //      status: 'finished',
      //  },
   ],
}


// BACK resp
interface BackResp 
  {
    ok: boolean,
    msg: string,
    entries: Entry[]
  }

  type BackRespEntry = {
    ok: boolean,
    msg: string,
    entry: Entry
  }

  interface Props {
    children?: React.ReactNode
  }

export const EntriesProvider: FC<Props> = ( {children} ) => {

  const [state, dispatch] = useReducer(entriesReducer, Entries_INITIAL_STATE );

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();


  const addNewEntry = async ( description: string ) => {
      
    try {
      
      const { data } = await entryApi.post <BackRespEntry>(`/entries`, { description });
      dispatch( {type:'[Entry] - Add-Entry', payload: data.entry});

    } catch (error) {

      console.log({error});
      
    }
  }

  const updatedEntry = async ( { _id, description, status }: Entry, snackbar = false  ) => {

    try {
      
      const { data } = await entryApi.put <BackRespEntry>(`/entries/${ _id }`, { description, status });
  
      dispatch({
        type:'[Entry] - Updated-Entry',
        payload: data.entry,
      })

      if( snackbar ){
        enqueueSnackbar('Update', {
          variant: 'success',
          autoHideDuration: 1500,
          anchorOrigin:{
              vertical: 'top',
              horizontal: 'right',
          }
        })
      }


    } catch (error) {
      console.log({error})
    }
  }

  const deleteEntry = async ( _id: string ) => {

    try {

      await entryApi.delete <BackRespEntry> (`/entries/${ _id }`);

      dispatch({
        type:'[Entry] - Delete-Entry',
        payload: _id,
      })
      
    } catch (error) {
      console.log({error})
    }
  }

  
  // CArgar la data de db
  const loadDataEntries = async ( ) => {
    
    const { data } = await entryApi.get<BackResp>(`/entries`);
    const { entries } = data
    // console.log(entries);

    dispatch({
      type:'[Entry] - Data-Entry-Loading',
      payload: entries
    })
  }

  useEffect(() => {
    loadDataEntries()
  }, [])
  

  return(
     <EntriesContext.Provider value={{
         ...state,
         addNewEntry,
         updatedEntry,
         deleteEntry,
        }}
      >
        { children }
     </EntriesContext.Provider>
  )

}