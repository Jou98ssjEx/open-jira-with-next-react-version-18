import { createContext } from "react";

interface ContextProps {
    sideMenuOpen: boolean;
    openSideMenu: () => void;
    closeSideMenu: () => void;

    isAddNewEntry: boolean;
    setIsAddNewEntry: (status: boolean ) => void;

    isDragging: boolean;
    setIsDragStart: ( ) => void;
    setIsDragEnd: ( ) => void;
}

export const UIContext = createContext( {} as ContextProps );

